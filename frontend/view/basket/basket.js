if(isBasketEmpty()) {
    let emptyBasket = document.createElement('p');
    emptyBasket.innerText = 'Votre panier est vide.';
    document.getElementById('panier').appendChild(emptyBasket);
}else {
    let basket = getBasket();

    let basketTable = document.createElement('table');
    basketTable.classList.add('table');
    let tableBody = document.createElement('tbody');
    basketTable.appendChild(tableBody);
    

    document.getElementById('panier').appendChild(basketTable);


    const fetchAllFurniture = fetch("http://localhost:3000/api/furniture")
    .then((response) => response.json())
    .then((data) => {
        basket.forEach(item => {
            let tr = document.createElement('tr');
            tableBody.appendChild(tr);
            const articleDetails = data.filter(d => d._id === item.id)[0]

            let tdPic = document.createElement('td');
            tdPic.innerHTML = `<img src="${articleDetails.imageUrl}" alt="" width="100px"></img>`;

            tr.appendChild(tdPic);

            let tdDetails = document.createElement('td');
            tdDetails.innerHTML = `<p><span class="font-weight-bold">${articleDetails.name}</span> / ${item.varnish}</p>`;
            tr.appendChild(tdDetails);
            
            let tdQty = document.createElement('td');
            tdQty.innerText = `${item.qty}`;
            tr.appendChild(tdQty);
            
            let tdPrice = document.createElement('td');
            tdPrice.innerText = `${articleDetails.price} €`;
            tr.appendChild(tdPrice);
        })
        let trTotal = document.createElement('tr');
        trTotal.innerHTML = `<td></td><td></td><td class="font-weight-bold">Total :</td><td class="font-weight-bold">${getBasketFullPrice()} €</td>`;
        tableBody.appendChild(trTotal);
    })


}
