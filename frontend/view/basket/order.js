console.log(getBasketIds())

async function postOrder(url = '', data = {}) {
    const response = await fetch("http://localhost:3000/api/furniture/order", {
        method: 'POST',
        headers: {
            'Content-Type': 'appplication/json'
        },
        body: JSON.stringify({
            contact: {
                firstName: 'John',
                lastName: 'Doe',
                address: '12 rue des jardins',
                city: 'Paris',
                email: 'jdoe@doe.org'
            },
            products: ['5be9cc611c9d440000c1421e']
        })
    });
    return response.json();
    // .then(res => {
    //         return res.json()
    //     })
    //     .then(data => console.log(data))
    //     .catch(error => console.log(error))
}

postOrder()
    .then(data => {
        console.log(data)
    })