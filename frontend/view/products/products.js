var furnitureInMemory = []

const fetchAllFurniture = fetch("http://localhost:3000/api/furniture")
    .then((response) => response.json())
    .then((data) => {
        console.log(data)
        data.forEach(furniture => {
            furnitureInMemory.push({id: furniture._id, name: furniture.name, imageUrl: furniture.imageUrl, description: furniture.description})
        });
    })

const renderAllFurniture = async () => {
    await fetchAllFurniture;
    furnitureInMemory.forEach(f => {
        document.getElementById('products').innerHTML += createProductCard(f)
    })
}

function createProductCard({id, name, imageUrl, description}) { // {} = like props in React / déstructuration /  on peut l'appeler avec en paramètre un Objet Furniture
	let cardHtml = `<div class="card" style="width: 18rem;" id="${id}">
										<img class="card-img-top" src="${imageUrl}" alt="image de ${name}">
										<div class="card-body">
											<h5 class="card-title">${name}</h5>
											<p class="card-text">${description}</p>
											<a href="../details/details.html?id=${id}" class="text-dark small">Détails</a>
										</div>
									</div>`
	return cardHtml;
}

renderAllFurniture()
