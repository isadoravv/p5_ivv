let params = new URLSearchParams(document.location.search.substring(1));
let productId = params.get("id");
console.log(productId);
getProductById(productId)

let quantityOFProducts = document.getElementById("quantity");
for (let q = 1; q <= 10; q++) {
    var quantity = document.createElement("option");
    quantity.textContent = q;
    quantity.value = q;
    quantityOFProducts.appendChild(quantity);
}

function getProductById(productId) {
	fetch("http://localhost:3000/api/furniture/"+productId)
	.then(function(res) {
		if (res.ok) {
			return res.json();
		}
	})
	.then(function(value) {
        console.log(value);
		document.getElementById("name").innerText = value.name;
		document.getElementById("description").innerText = value.description;
		document.getElementById("prodImg").setAttribute("src", value.imageUrl);
		document.getElementById("price").innerText = value.price + " €";

        let prodVarnishesSelect = document.getElementById("varnishSelect");
        for (let v = 0; v < value.varnish.length; v++) {
        	const option = value.varnish[v];
        	var varnishOption = document.createElement("option");
        	varnishOption.textContent = option;
        	varnishOption.value = option;
        	prodVarnishesSelect.appendChild(varnishOption);
        }

	})
	.catch(function(err) {
		// Handle error
	});
};


const btnAddBasket = document.getElementById("addBasket");
btnAddBasket.addEventListener('click', function(event) {
    event.preventDefault();
    const quantity = parseInt(document.getElementById("quantity").value);
    const price = parseInt(document.getElementById("price").innerHTML.slice(0,-2));
    const varnishSelect = document.getElementById("varnishSelect").value;
    const toAdd = {productId, varnishSelect, quantity, price};
    console.log(toAdd);
    addToBasket(productId, varnishSelect, quantity, price);
    displayNumberOfItemsInBasket();
})
