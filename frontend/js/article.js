const furnitures = [
  {
    "varnish": [
      "Tan",
      "Chocolate",
      "Black",
      "White"
    ],
    "_id": "5be9cc611c9d440000c1421e",
    "name": "Cross Table",
    "price": 59900,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "imageUrl": "oak_1.jpg"
  },
  {
    "varnish": [
      "Dark Oak",
      "Light Oak"
    ],
    "_id": "5beaadda1c9d440000a57d98",
    "name": "Coffee Table",
    "price": 89900,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "imageUrl": "oak_2.jpg"
  },
  {
    "varnish": [
      "Dark Oak",
      "Mahogany"
    ],
    "_id": "5beab2061c9d440000a57d9b",
    "name": "Vintage Chair",
    "price": 79900,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "imageUrl": "oak_5.jpg"
  }
]

var basket = []

class Article {
    constructor(id, varnish, quantity) {
        this.id = id;
        this.varnish = varnish;
        this.quantity = quantity;
    }
}
basket.push({id:'4a3', varnish:'Dark Oak', qty: 1})
basket.push({id:'5e2', varnish:'Dark Oak', qty: 3})
basket.push({id:'5e2', varnish:'Mahogany', qty: 2})

console.log(basket.filter(i => i.id === '5e2' && i.varnish == 'Dark Oak'))
console.log(basket.map(i => i.id === '5e2' && i.varnish == 'Dark Oak'))
console.log(basket.find(i => i.id === '5e2' && i.varnish == 'Dark Oap'))

basket.forEach(i => {
    if(i.id == '5e2' && i.varnish =='Dark Oak'){ i.qty+=2 };
})

console.log(basket)