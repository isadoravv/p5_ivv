function addToBasket(productId, productVarnish, productQuantity, productPrice) {
    let basket = getBasket();

    if(!isBasketEmpty()) {
        articlesWithSameIdandVarnish = basket.filter(item => item.id === productId && item.varnish === productVarnish)
        if(articlesWithSameIdandVarnish.length > 0) {
            basket.forEach(item => {
                if(item.id == productId && item.varnish == productVarnish){ item.qty += productQuantity}
            })
        }else {
            basket.push({id: productId, varnish: productVarnish, qty: productQuantity, price: productPrice});
        }
    }else {
        basket.push({id: productId, varnish: productVarnish, qty: productQuantity, price: productPrice});
    }
    console.log('tosave', basket);
    saveBasket(basket);
}

function getBasket() {
    let basket = localStorage.getItem("basket");
    if(basket == null){
        return [];
    }else {
        return JSON.parse(basket);
    }
}

function isBasketEmpty() {
    let basket = getBasket();
    if(basket.length ===0) {
        return true;
    }else if (basket.length === 1 && basket[0].id === undefined){
        return true;
    }
    return false;
}

function numberOfItemsInBasket() {
    var nbOfItems = 0;
    let basket = getBasket();
    basket.forEach(item => {
        nbOfItems += item.qty;
    })
    return nbOfItems;
}

function getBasketIds() {
    return getBasket().map(item => item.id);
}

function saveBasket(basket) {
    localStorage.setItem("basket", JSON.stringify(basket));
}

function getBasketFullPrice() {
    var price = 0;
    let basket = getBasket();
    basket.forEach(item => {
        price += (item.qty * item.price);
    })
    return price;
}